from glob import glob
from pathlib import Path

import json
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
import subprocess
import sys

sns.set(rc={ 'figure.figsize': (15, 12) })
sns.set(font_scale=1.5)

def loc_plot(directory, n=None):
    dirs_lines = []
    directories = glob(str(directory) + "/*/", recursive=True)

    if n is None:
        n = len(directories)

    for directory in directories:
        tokei = subprocess.run(
            ["tokei", directory, "--output", "json"],
            stdout=subprocess.PIPE,
            encoding="utf-8"
        )
        tokei_json = json.loads(tokei.stdout)
        lines = tokei_json["Total"]["code"]
        dirs_lines.append(lines)

    d = { 'Directories': list(map(lambda d : Path(d).stem, directories)), 'Lines': dirs_lines }
    df = pd.DataFrame(d, columns=['Directories', 'Lines'])

    ax = sns.barplot(
        x='Lines',
        y='Directories',
        data=df,
        order=df.sort_values('Lines', ascending=False).Directories.head(n)
    )

    plt.show()

def main():
    if len(sys.argv) == 2:
        loc_plot(sys.argv[1])
    elif len(sys.argv) == 3:
        loc_plot(sys.argv[1], int(sys.argv[2]))
    else:
        print("python3 lines_of_code.py <path> [<n>]")

if __name__ == '__main__':
    main()
