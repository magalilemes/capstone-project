from glob import glob
from pathlib import Path

import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
import subprocess
import sys

sns.set(rc={ 'figure.figsize': (1, 1) })
sns.set(font_scale=1.5)

def commits_amount(contributor, linux_repo):

    command_template = "git log --oneline"
    if contributor == "external":
        command_template += " --author='^(?!(.*@amd[.]com))' --perl-regexp"
    elif contributor == "amd":
        command_template += " --author='^(.*@amd[.]com)' --perl-regexp"

    commits = []

    for year in range(2019, 2023):
        command = command_template
        command += " --after=\"" + str(year) + "-01-01\"" + " --until=\"" + str(year) + "-12-31\""
        command += " -- drivers/gpu/drm/amd/display | wc -l"

        commits_out = subprocess.run(
            command,
            cwd=linux_repo,
            encoding="utf-8",
            shell=True,
            stdout=subprocess.PIPE,
        )

        commits.append(int(commits_out.stdout))

    return {"year": list(range(2019, 2023)),
            "contributor": [contributor]*4,
            "commits": commits}

def main():
    if len(sys.argv) != 2:
        print("python3 kunit_usage.py <linux path>")
        return

    linux_repo = sys.argv[1]
    df1 = pd.DataFrame(commits_amount("amd", linux_repo))
    df = df1.append(pd.DataFrame(commits_amount("external", linux_repo)), ignore_index=True)

    print(df)

    sns.factorplot(x='year', y='commits', hue='contributor', data=df, kind='bar')

    plt.show()


if __name__ == '__main__':
    main()
