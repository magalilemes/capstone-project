from glob import glob
from pathlib import Path

import matplotlib.pyplot as plt
import re
import seaborn as sns
import subprocess
import sys

sns.set(rc={ 'figure.figsize': (22, 12) })
sns.set(font_scale=1.5)

def subsystems_with_kunit(linux_repo):
    subsystems_kunit = set()

    paths = Path(linux_repo).rglob('*')
    for path in paths:
        if path.is_file():
            with open(path, 'r', encoding="ISO-8859-1") as f:
                    if "kunit/test.h" in f.read():
                        maintainer = subprocess.run(
                            ["./scripts/get_maintainer.pl", "--subsystem", "--no-email", "--no-l", path],
                            stdout=subprocess.PIPE,
                            cwd=linux_repo,
                            encoding="utf-8")
                        subsystem = re.match("[^\n]+", maintainer.stdout).group(0)

                        subsystems_kunit.add(subsystem)
    return subsystems_kunit

def main():
    if len(sys.argv) != 2:
        print("python3 kunit_usage.py <linux path>")
    else:
        linux_repo = sys.argv[1]
        linux_tags = subprocess.run(
        [
            "git", "tag", "--list", 'v[5-9].[0-9]*',
            "--sort=version:refname"
        ],
        cwd=linux_repo,
        stdout=subprocess.PIPE,
        encoding="utf-8",
    )
    tags = []

    for t in linux_tags.stdout.split("\n")[:-1]:
        final = re.match("v[0-9].[0-9]*$", t)
        if final is not None:
            tags.append(final.group(0))

    kunit_usage = []

    for tag in tags[4:]:
        subprocess.run(
            ["git", "checkout", tag],
            cwd=linux_repo,
            encoding="utf-8",
        )

        kunit_usage.append(len(subsystems_with_kunit(linux_repo)))

    ax = sns.lineplot(x=tags[4:], y=kunit_usage, marker='o')
    ax.set_xlabel("Version", fontsize=15)
    ax.set_ylabel("Number of subsystems", fontsize=15)

    plt.show()

if __name__ == '__main__':
    main()
