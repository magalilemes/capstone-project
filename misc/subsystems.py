from pathlib import Path
import re
import subprocess
import sys

def subsystems_with_kunit(linux_repo):
    subsystems_kunit = set()
    paths = Path(linux_repo).rglob('*')

    for path in paths:
        if path.is_file():
            with open(path, 'r', encoding="ISO-8859-1") as f:
                    if "kunit/test.h" in f.read():
                        maintainer = subprocess.run(
                            ["./scripts/get_maintainer.pl", "--subsystem", "--no-email", "--no-l", path],
                            stdout=subprocess.PIPE,
                            cwd=linux_repo,
                            encoding="utf-8")
                        subsystem = re.match("[^\n]+", maintainer.stdout).group(0)

                        subsystems_kunit.add(subsystem)
    return subsystems_kunit

def main():
    print(subsystems_with_kunit(sys.argv[1]))

if __name__ == '__main__':
    main()
