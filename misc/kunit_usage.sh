#!/bin/bash

readarray -d '' -t files < <(grep -RZl "kunit/test.h")
for file in "${files[@]}"; do
	./scripts/get_maintainer.pl --subsystem "$file" | grep -E ^[^a-z]*$ | head -n 1
done
