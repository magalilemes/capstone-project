%!TeX root=../tese.tex
%("dica" para o editor de texto: este arquivo é parte de um documento maior)
% para saber mais: https://tex.stackexchange.com/q/78101

\chapter{Results and Discussion}
\label{chap:results}

This chapter covers the contributions made to the Linux kernel: contributions
related to introducing unit tests to the AMD display driver and other more
general contributions not wholly linked to fulfilling this goal. We also
explain how test cases were devised and discuss design choices related to
KUnit's adoption into AMDGPU.

\section{General Contributions}

As part of the process of getting used to the Linux kernel development
workflow, especially concerning the AMD display driver subsystem, we made a few
first contributions not directly related to adding unit tests. These were
primarily the correction of compilation warnings and the removal of duplicated
and unused code. Those contributions are listed in
Table~\ref{fig:kernel_contributions}, where the numbers in green represent the
number of lines added, and those in red the number of lines removed.

\begin{figure}[ht]
\begin{center}
\adjustbox{max width=\textwidth}{%
  \begin{tabular}{| l | l | l | l|}
    \hline
    Age & Commit Message & Files & Lines \\ \hline
    2022-02-02 & drm/amd/display: Use NULL pointer instead of plain integer & 1 & \changes{1}{1} \\ \hline

    2022-02-24 & drm/amd/display: Adjust functions documentation & 1 & \changes{3}{3} \\ \hline

    2022-02-24 & drm/amd/display: Add conditional around function & 1 & \changes{1}{3} \\ \hline

    2022-02-24 & drm/amd/display: Use NULL instead of 0 & 3 & \changes{1}{4} \\ \hline

    2022-02-24 & drm/amd/display: Turn functions into static & 3 & \changes{18}{3} \\ \hline

    2022-05-04 & powerpc: Fix missing declaration of [en/dis]able\_kernel\_altivec() & 1 & \changes{0}{9} \\ \hline

    2022-08-10 & drm/amd/display: remove DML Makefile duplicate lines & 1 & \changes{2}{0} \\ \hline

    2022-08-10 & drm/amd/display: make variables static & 4 & \changes{6}{3} \\ \hline

    2022-08-10 & drm/amd/display: remove header from source file & 2 & \changes{2}{2} \\ \hline

    2022-08-10 & drm/amd/display: include missing headers & 3 & \changes{0}{5} \\ \hline

    2022-08-22 & drm/amd/display: drm/amd/display: remove unused header & 1 & \changes{34}{0} \\ \hline
  \end{tabular}}
\end{center}
\caption{Table listing contributions made to the Linux kernel}
\label{fig:kernel_contributions}
\end{figure}

\section{Implementing Unit Tests}

Inside the AMD display driver directory structure, we mainly focused on writing
unit tests for the Display Mode Library (DML), part of the Display Core (DC)
component, due to its mathematical nature. This library is responsible for
dealing with ``clock, watermark, and bandwidth calculations for
DCN''\footnote{https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=061bfa06a4}.
Apart from DML, we also built tests for the fixed31\_32 and DMUB libraries,
both part of the Display Core component as well. For thinking and devising test
cases, we mostly relied on inspecting \textbf{code coverage},
\textbf{equivalence partitioning}, and \textbf{boundary-value analysis}
techniques, described in Section~\ref{techniques-build-tests}. We also used
past regressions in the code we evaluated as references to build test cases.

%% worth mentioning we sent an RFC with initial tests for bw_fixed, then v1, v2..?
%% RFC: https://lists.freedesktop.org/archives/amd-gfx/2022-June/080181.html
%% v1: https://lists.freedesktop.org/archives/amd-gfx/2022-August/082618.html
%% v2: https://lists.freedesktop.org/archives/amd-gfx/2022-August/083631.html

\subsection{Using Test Design Techniques}

The first file we explored was \textit{bw\_fixed.c}, which can be found at
\textit{drivers/gpu/drm/amd/display/dc/dml/calcs}. It mainly contains basic
mathematical functions, such as calculating the absolute value and the ceiling
of given numbers and operations that deal with fixed-point arithmetic. The
following two subsections discuss how we developed test cases for two functions
from this file using equivalence partitioning and boundary-value analysis.

% abs_i64
\subsubsection{Function \texttt{abs\_i64()}}

The first function we explored was \texttt{abs\_i64}, defined as shown in
Figure~\ref{fig:abs_i64_definition}.  This function returns an
\texttt{uint64\_t} with the absolute value of the parameter it receives.

\begin{figure}[ht]
  \begin{lstlisting}[language=C]
  static uint64_t abs_i64(int64_t arg)
  {
    if (arg >= 0)
      return (uint64_t)(arg);
    else
      return (uint64_t)(-arg);
  }
  \end{lstlisting}
\caption{abs\_i64() definition}
\label{fig:abs_i64_definition}
\end{figure}

We started off by analyzing the argument \texttt{arg}: an \texttt{int64\_t},
alias for \texttt{signed long long}, which can range from $-(2^{63})$ to
$2^{63} - 1$. As a first approach, we wanted to test how the function behaves
when it deals with these border cases. Since any other values outside the range
mentioned above can lead to an integer overflow and the function does not
handle these cases, we did not test them.

\begin{figure}[ht]
  \begin{center}
    \begin{tikzpicture}[scale=7]
      \draw[thick] (-0.1,0) -- (0.9,0);
      \foreach \x/\xtext in {0/$-2^{63}$,0.4/0,0.8/$2^{63}-1$}
      \draw[thick] (\x,0.5pt) -- (\x,-0.5pt) node[below] {\xtext};
      \draw[[-), ultra thick, blue] (0,0) -- (0.399,0);
      \draw[{[-]}, ultra thick, red] (0.4,0) -- (0.8,0);
    \end{tikzpicture}
  \end{center}
  \caption{Equivalence partitions for abs\_i64()}
  \label{fig:abs_i64_classes}
\end{figure}

By looking at the \texttt{if}-statements, we could determine two equivalence
classes: one for values greater than or equal to 0 and another for values less
than 0. In the first scenario, the function does not make any change to the
input, and its return value is the input integer, as it is, cast to
\texttt{uint64\_t}; in the second scenario, the function takes the input and
returns its opposite, also cast to \texttt{uint64\_t}. As the function changes
its behavior around 0, we set it as one boundary. We also have the boundaries
defined based on the argument type, leaving us with the two categories depicted
in Figure~\ref{fig:abs_i64_classes}.

Finally, we wanted to test the values on the border and the values close to it
so that more than one representative of the partition class would get tested.
We ended up with the following cases for \texttt{arg}:
% = MIN\_I64
% = MAX\_I64
\begin{itemize}
  \item $-2^{63} $, the lower bound of \texttt{int64\_t}
  \item $ 0 $, value where the function changes its behavior
  \item $ 1 $, value close to one equivalence class boundary
  \item $2^{63} - 1 $, the upper bound of \texttt{int64\_t}
\end{itemize}

Figure~\ref{fig:abs_i64_tests} shows how we can build a test for the values
above using KUnit features.  

\begin{figure}[ht]
  \begin{lstlisting}[language=C]
  /**
  * abs_i64_test - KUnit test for abs_i64
  * @test: represents a running instance of a test.
  */
  static void abs_i64_test(struct kunit *test)
  {
    KUNIT_EXPECT_EQ(test, 0ULL, abs_i64(0LL));
    KUNIT_EXPECT_EQ(test, 1ULL, abs_i64(-1LL));

    /* Argument type limits */
    KUNIT_EXPECT_EQ(test, (uint64_t)MAX_I64, abs_i64(MAX_I64));
    KUNIT_EXPECT_EQ(test, (uint64_t)MAX_I64 + 1, abs_i64(MIN_I64));
  }
  \end{lstlisting}
  \caption{KUnit tests cases for \text{abs\_i64()}}
  \label{fig:abs_i64_tests}
\end{figure}

\subsubsection{Function \texttt{bw\_floor2()}}
\label{bw-floor2}

This function takes two arguments of type \texttt{bw\_fixed} (whose structure
definition is found in Figure~\ref{fig:bw_fixed_definition}): the first one,
\texttt{arg}, is the value that will be rounded down; the second one,
\texttt{significance}, is the multiple to which \texttt{arg} will be rounded.
The function definition is shown in Figure~\ref{fig:bw_floor_2_definition}.

\begin{figure}[ht]
  \begin{lstlisting}[language=C]
  struct bw_fixed {
    int64_t value;
  };
  \end{lstlisting}
  \caption{bw\_fixed definition}
  \label{fig:bw_fixed_definition}
\end{figure}

\begin{figure}[ht]
  \begin{lstlisting}[language=C]
  struct bw_fixed bw_floor2(
    const struct bw_fixed arg,
    const struct bw_fixed significance)
  {
    struct bw_fixed result;
    int64_t multiplicand;

    multiplicand = div64_s64(arg.value, abs_i64(significance.value));
    result.value = abs_i64(significance.value) * multiplicand;
    ASSERT(abs_i64(result.value) <= abs_i64(arg.value));
    return result;
  }
  \end{lstlisting}
  \caption{bw\_floor2() definition}
  \label{fig:bw_floor_2_definition}
\end{figure}

The first step we followed to build test cases for this function was defining
the partition classes. To do so, we began by analyzing the parameters: both are
of type \texttt{struct bw\_fixed}, a structure consisting of only one member,
\texttt{value}, of type \texttt{int64\_t}.

\begin{figure}[ht]
  \begin{center}
    \begin{tabular}{ c | c| c   }
      \hline
      arg.value & significance.value & result.value \\
      \hline
      \rowcolor{blue!20} - & - & - \\
      \rowcolor{yellow!20} 0 & + & 0 \\
      \rowcolor{yellow!20} 0 & - & 0 \\
      \rowcolor{green!20} + & + & + \\
      \rowcolor{green!20} + & - & + \\
      \hline
     \end{tabular}
  \end{center}
  \caption{Equivalence partitions for \texttt{bw\_floor2()}}
  \label{fig:bw_floor2_classes}
\end{figure}

The variable \texttt{value} could be either positive, negative, or zero. Taking into account
the function context, we noticed that, as per lines 8 and 9 of the function
definition (Figure~\ref{fig:bw_floor_2_definition}), the sign of the variable
\texttt{significance}'s \texttt{value} does not make a difference,
since the function only uses its absolute value. On the other hand, the sign of
\texttt{arg} \texttt{value} does interfere with the function output. From here,
we defined three equivalence classes, shown in
Figure~\ref{fig:bw_floor2_classes}: one that outputs positive values, another
that outputs negative ones, and another that outputs zero. We exercised the
border values on the range of the arguments in a similar manner as done for the
function \texttt{bw\_floor2()} described above in \ref{bw-floor2}.

Furthermore, we added more than one test case for some equivalence classes in a
way to document and explain to developers, who might work with this function,
how it works. Figure~\ref{fig:bw_floor2_tests} shows the final result with all
the tests we built for \texttt{bw\_floor2()}.


\begin{figure}[H]
  \begin{lstlisting}[language=C]
  /**
  * bw_floor2_test - KUnit test for bw_floor2
  * @test: represents a running instance of a test.
  */
  static void bw_floor2_test(struct kunit *test)
  {
    struct bw_fixed arg;
    struct bw_fixed significance;
    struct bw_fixed res;

    /* Round 10 down to the nearest multiple of 3 */
    arg.value = 10;
    significance.value = 3;
    res = bw_floor2(arg, significance);
    KUNIT_EXPECT_EQ(test, 9, res.value);

    /* Round 10 down to the nearest multiple of 5 */
    arg.value = 10;
    significance.value = 5;
    res = bw_floor2(arg, significance);
    KUNIT_EXPECT_EQ(test, 10, res.value);

    /* Round 100 down to the nearest multiple of 7 */
    arg.value = 100;
    significance.value = 7;
    res = bw_floor2(arg, significance);
    KUNIT_EXPECT_EQ(test, 98, res.value);

    /* Round an integer down to its nearest multiple should return itself */
    arg.value = MAX_I64;
    significance.value = MAX_I64;
    res = bw_floor2(arg, significance);
    KUNIT_EXPECT_EQ(test, MAX_I64, res.value);

    arg.value = MIN_I64;
    significance.value = MIN_I64;
    res = bw_floor2(arg, significance);
    KUNIT_EXPECT_EQ(test, MIN_I64, res.value);

    /* Value is a multiple of significance, result should be value */
    arg.value = MAX_I64;
    significance.value = MIN_I64 + 1;
    res = bw_floor2(arg, significance);
    KUNIT_EXPECT_EQ(test, MAX_I64, res.value);

    /* Round 0 down to the nearest multiple of any number should return 0 */
    arg.value = 0;
    significance.value = MAX_I64;
    res = bw_floor2(arg, significance);
    KUNIT_EXPECT_EQ(test, 0, res.value);

    arg.value = 0;
    significance.value = MIN_I64;
    res = bw_floor2(arg, significance);
    KUNIT_EXPECT_EQ(test, 0, res.value);
  }
  \end{lstlisting}
  \caption{bw\_floor2() test case}
  \label{fig:bw_floor2_tests}
\end{figure}



\summarybox{Lessons and Recommendations}{red!20}{white}{ % Or Summary?
After sending out the \textit{bw\_fixed.c} tests for review, an integer
overflow problem in the code we were testing was detected and
reported\footnote{https://lists.freedesktop.org/archives/amd-gfx/2022-August/082649.html}. This report is shown in Appendix~\ref{app:overflow-warning}.
Subsequently, a
patch\footnote{https://lists.freedesktop.org/archives/amd-gfx/2022-August/082746.html}
fixing the issue was sent, and
merged\footnote{https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=6ae0632d17}.  

All things considered, we summarize these lessons:

\begin{itemize}

	\item Following \textbf{techniques for designing test cases} is quite efficient, especially when not knowing, at first, how to select values for testing. In our case, adopting those techniques proved extremely helpful for the small functions we wrote tests for. 

	\item For the test cases above, we are essentially using the same test and only changing the function input values. This may be a good call for \textbf{parameterized testing}, as suggested in the mailing list discussion, which can be checked in Appendix~\ref{app:param-suggestion}.

	\item Tests can also serve as an alternative way to \textbf{document} the behavior of a function.

\end{itemize}
}  


\subsection{Covering Regressions}

A regression happens when the software or part of it starts behaving
unintendedly or stops working after adding a new change. For both functions
described below, we searched for events of regressions in the code we intended
to test, so we could build test cases that cover those specific situations.

\subsubsection{Function \texttt{dcn21\_update\_bw\_bounding\_box()}}

After building tests for all functions from \textit{bw\_fixed.c}, we moved on
to build test cases for different DML files from the DCN 2.0 architecture,
which corresponds to the architecture from the GPUs we had at our disposal, at
\textit{drivers/gpu/drm/amd/display/dc/dml/dcn20}.

We built tests for this function based on a real-life event, using a
Test-Driven Development (TDD) approach to show how unit tests can be integrated
into the kernel development process.

A TDD approach can also be adapted when finding a bug or regression in the
code. The flowchart in Figure~\ref{fig:tdd-regression} illustrates one way to
do this, also summarized in the list of steps below.

\begin{itemize}

	\item After finding a regression, the developer has to investigate and identify why and how it is happening.

	\item The developer, then, writes one or more test cases that will naturally fail due to the regression but would pass in a regression-free code.
 
	\item The regression is then fixed. 

	\item The tests should be run and passed.

\end{itemize}

\begin{figure}[ht]
  \begin{tikzpicture}
    \node [terminator] at (0,0) (start) {\textbf{Find a regression}};
    \node [process] at (4,0) (investigate) {Investigate it};
    \node [process] at (8,0) (build) {Build failing test(s)};
    \node [process] at (12,0) (run) {Fix regression};
    \node [decision] at (12,-3) (decision) {Are the tests passing?};
    \node [process] at (8,-3) (error) {Rework code};
    \node [process, fill=green!20] at (12,-6) (success) {Success};
    \node [terminator] at (8,-6) (end) {\textbf{End}};
    \node[draw=none] at (9.75, -2.5) (no) {No};
    \node[draw=none] at (11.5, -5) (yes) {Yes};
    \path [connector] (start) -- (investigate);
    \path [connector] (investigate) -- (build);
    \path [connector] (build) -- (run);
    \path [connector] (run) -- (decision);
    \path [connector] (decision) -- (error);
    \path [connector] (decision) -- (success);
    \path [connector] (error) |- (decision.north);
    \path [connector] (success) -- (end);
    \end{tikzpicture}
  \caption{Diagram showing how to apply TDD to fix regressions}
  \label{fig:tdd-regression}
\end{figure}
% inspired a bit by: https://www.tutisani.com/software-architecture/bug-fix-via-tdd.html



To apply this technique in the AMD display driver subsystem, we first
identified a regression in the \texttt{dcn21\_update\_bw\_bounding\_box()}
function. At one point, this function exceeded the defined stack size for it,
which caused a compilation warning. A first
attempt\footnote{https://lists.freedesktop.org/archives/amd-gfx/2022-June/080004.html}
to fix this warning was made, and it fixed the problem. This solution, though,
did not preserve the function behavior -- that is, the fix caused the function
output to be different from what it was before the fix --, causing a
regression. A new
patch\footnote{https://lists.freedesktop.org/archives/amd-gfx/2022-June/080214.html}
was then sent to fix both the stack size warning as well as the previously
introduced regression. We can summarize this set of events into three parts:  

\begin{enumerate}
	\item \textbf{Problem}: The stack size of the function triggered a compilation warning.
	\item \textbf{Regression}: A patch fixed the problem, but it changed the function behavior.
	\item \textbf{Fix}: A patch was sent to fix the regression and the initial problem.
\end{enumerate}  

We decided to use this occurrence as a background for devising a test case for
the \texttt{dcn21\_update\_bw\_bounding\_box()} function. To do so, we first
investigated the root cause of the introduced regression. After that, we built
a test case that would fail due to the circumstances brought by the tough patch
but otherwise would pass if we just reverted it. Then, we applied the patch
that fixed the regression and reran the tests to check if the problem was fixed
by asserting if the tests had passed. This was an attempt to show by example
how developers, be they from the Linux kernel or not, can use tests to fix bugs
and regressions in a sophisticated manner.  

Another important thing to highlight is that this function has large structures
as arguments, so we would stumble upon a stack-size warning when writing the
test function in the traditional KUnit manner. To solve that, we decided to
take advantage of the parameterized testing feature provided by KUnit, which,
instead of using heap memory, allocates the necessary memory dynamically,
avoiding subsequent stack size warnings and also making the process of adding
new test cases for the function easier.  


\subsubsection{Function \texttt{populate\_subvp\_cmd\_drr\_info()}}
\label{sss:populate-subvp-cmd-drr-info}
We also found a regression in the \texttt{populate\_subvp\_cmd\_drr\_info()}
for which we wrote a test case, similarly to the approach applied to devise the
test case for the \texttt{dcn21\_update\_bw\_bounding\_box()} function. This
function is found under
\textit{drivers/gpu/drm/amd/display/dc/dc\_dmub\_srv.c}.

In this case, a
patch\footnote{https://lore.kernel.org/amd-gfx/20220630191322.909650-3-Rodrigo.Siqueira@amd.com/}
adding a new feature ended up introducing compilation warnings for 32-bit
architectures due to the floating-point operations in the code. A new
patch\footnote{https://lore.kernel.org/amd-gfx/20220708052650.1029150-1-alexander.deucher@amd.com/}
was then sent to fix the previous problem, but it turned out to change the
function behavior by zeroing some values from a structure it should not have.
The summary of events is listed as follows:  

\begin{enumerate}

	\item \textbf{Problem}: Floating-point operations caused 32-bit compilation errors.

	\item \textbf{Regression}: A patch attempted to fix introduced the problem but zeroed all values in the structure the function is supposed to populate. 

\end{enumerate}  

To build the test case, we studied the expected behavior of the function before
the regression was introduced. After that, we devised cases using the values
the function produced when reverting the commit that brought the regression, so
all the tests would pass. We also applied parameterized testing.  

\summarybox{Lessons and Recommendations}{red!20}{white}{

\begin{itemize}

	\item Tests can also serve for \textbf{documenting} a function past of regressions.

	\item One interesting source for building test cases is \textbf{covering past regressions} in the code.

	\item For eventual regressions that may be found in the code, we strongly recommend \textbf{writing unit tests} that cover them since the developer who will fix it understands and can think of test cases where that piece of code is not working. 

\end{itemize}
}  


\subsection{Test Coverage}

\textbf{Test coverage}, or code coverage, is one way to estimate how much source
code is covered by tests, measuring the amount of source code that was hit when
the tests that cover it are run. Below, in Figure~\ref{fig:tested-directories}
we present the folders and files -- in purple and yellow, respectively --
covered by tests.

\begin{figure}[ht]
\begin{tikzpicture}[sibling distance=12em,
  every node/.style = {shape=rectangle, rounded corners,
    draw, align=center}]]
  \node[diretorio] {drivers/gpu/drm/amd/display/dc}
    child { node[diretorio] {basics}
      child { node[arquivo] {fixpt31\_32.c} } }
    child { node[arquivo] {dc\_dmub\_srv.c} }
    child { node[diretorio] {dml}
      child[level distance=6em] { node[diretorio] {dcn20}
        child { node[arquivo] {dcn20\_fpu.c} }
        child { node[arquivo] {display\_mode\_vba\_20.c} }
        child { node[arquivo] {display\_rq\_dlg\_calc\_20.c} } }
      child { node[diretorio] {calcs}
        child { node[arquivo] {bw\_fixed.c} } } };
\end{tikzpicture}
\caption{Tree showing files for which tests were written}
\label{fig:tested-directories}
\end{figure}

Using \textit{gcov}, a tool that generates code coverage reports, alongside
KUnit, we can obtain information about the percentage of lines and functions
covered, in addition to the specific lines that were covered or not.
Figures~\ref{fig:cov-dmub},~\ref{fig:cov-dml}, and~\ref{fig:cov-fixed31-32}
show the results we got.  

\begin{figure}[H]
  \includegraphics[scale=0.5]{figuras/coverage_dml.png}
  \centering
  \caption{Report showing code coverage from DML}
  \label{fig:cov-dml}
\end{figure}

\begin{figure}[H]
  \includegraphics[scale=2]{figuras/coverage_dmub.png}
  \centering
  \caption{Report showing code coverage for DMUB}
  \label{fig:cov-dmub}
\end{figure}

\begin{figure}[H]
  \includegraphics[scale=2]{figuras/coverage_fixed_3132.png}
  \centering
  \caption{Report showing code coverage from fixed31\_32}
  \label{fig:cov-fixed31-32}
\end{figure}

As stated before, we mostly focused on writing tests for the DML folder. At
first glance, Figure~\ref{fig:cov-dml} indicates that little line and function
coverages were achieved in that area. When analyzing those numbers, though, we
have to keep in mind that this directory is huge in number of lines and many of
the functions need to be rewritten and split so that they can be tested.
Nevertheless, we have tests that cover 20 functions from the DML folder.

Outside of the DML, in Figure~\ref{fig:cov-dmub} we can see that 10.3\% of
the runnable lines from the \texttt{dc\_dmub\_srv.c} file were executed when our
tests are run. We achieved that by testing 1 out of the 26 functions of the
file, in this case, the function \texttt{populate\_subvp\_cmd\_drr\_info()},
discussed in the topic \ref{sss:populate-subvp-cmd-drr-info}. As for the
\textit{fixed31\_32.c} file, which handles fixed-point operations,
Figure~\ref{fig:cov-fixed31-32} shows that our tests covered 48.7\% of the lines
by testing 27.8\% of the functions.

\subsection{Design Choices}

We placed all the tests in a \textit{tests} folder under the \textit{display}
directory since this is where all the files we tested live. Furthermore, under
\textit{tests}, we replicated the \textit{display} folder structure, following
the structure of the files we tested. For example, the tests for the
\textit{drivers/gpu/drm/amd/display/dc/dml/dcn20/dcn20\_fpu.c} file are in
\textit{drivers/gpu/drm/amd/display/tests/dc/dml/dcn20/dcn20\_fpu\_test.c}.

% CONFIG_AMD_DC_BASICS_KUNIT_TEST=y
% CONFIG_AMD_DC_KUNIT_TEST=y
% CONFIG_DCE_KUNIT_TEST=y
% CONFIG_DML_KUNIT_TEST=

\subsubsection{Test Module}

When compiling the Linux kernel, we can choose whether the modules will be
built-in or loadable. In the first case, it means that whenever the computer is
booted, the module will be automatically loaded. In the second case, we can
choose, at runtime, when to load and when to unload the module.  

The KUnit tests we wrote can either be run as a module or be built-in, both as
part of the AMDGPU module. They are also hardware-independent and can run
either on virtual machines or on bare metal\footnote{System with physical
hardware}.  

When we first approached the task of using KUnit for writing tests for the AMD
display driver, we originally wanted the tests to be isolated in one single
module, meaning that they could be loaded and unloaded without necessarily
depending on the whole AMDGPU module.  

KUnit allows tests to be run as a single module, but if we want to use
functions from other modules, as in any kernel module, they need to be
available in the kernel namespace, meaning that they have to be explicitly
exported. That is not the case with the functions in the AMD display driver, so
we did not have an out-of-the-box solution for defining an exclusive module for
tests.  


\subsubsection{Static Functions}

% subitem: static functions To test, or not to test
When writing the unit tests, we focused on testing public functions. As we
progressed, the public functions that we had not tested yet were mostly
functions with high cyclomatic complexity\footnote{A metric that expresses the
number of possible paths that a piece of code can take~\citep{1702388}},
indicating that we would have to write many equally complex test cases if we
wanted to cover those functions well. That left us with the choice of either (i)
creating relatively incomplete tests that might cover only a couple of outcomes,
(ii) explore the simple static functions that were left or (iii) rewrite the
public functions so that they can be more approachable to testing.

% TODO (Paulo): Se der, ou na versão final, tem que explicar o que implicar ter uma alta complexidade ciclomática (o que deveria levar a uma refatoração dessas funções, inclusive para facilitar a escrita dos testes)

\begin{figure}[ht]
\begin{lstlisting}[firstnumber=1001, language=C]
#if IS_ENABLED(CONFIG_AMD_DC_KUNIT_TEST)
#include "../kunit/dc/dc_dmub_srv_test.c"
#endif
\end{lstlisting}
\caption{Appending test file into source file}
\label{fig:include-file}
\end{figure}

We chose the option 2 (ii) and decided to test static functions, especially
since we were interested in one static function with a regression history. As
mentioned in the subsection~\ref{building-tests}, the KUnit way for testing
static functions means appending the test file source code into the file we are
testing, illustrated in Figure~\ref{fig:include-file}. This approach may not
please everyone, as a response in Appendix~\ref{app:opinion} exemplifies, since,
when testing a file, we should try not to modify the original file as much as
possible.


Finally, we chose a mixed approach for the tests: we appended the source code
of the test file only in the files with static functions we wanted to test; the
files whose tested functions were not static remained unchanged.

\section{Summary}

We can argue that unit testing is a consolidated practice in userspace
applications and, compared to lower-level applications like the Linux kernel,
there are also similarities and different challenges.

One of our first concerns was the need to mock devices, which proved
unnecessary as we wrote the tests for self-contained functions. This allows the
tests to be run on various machines; it does not depend on the specific GPU for
which the code is written. Going further, we followed techniques to write test
cases also used in userspace software. Finally, running the tests can be as
easy as running a script or more challenging if the user chooses to compile and
install the kernel with tests coupled in it.

Now that the overall structure for unit testing is ready, it is quite
straightforward to follow the examples provided by the available tests and add
new tests for both beginners and more experienced developers. There are many
opportunities to apply unit testing: when developing a new feature, the
developer can take advantage of TDD; if a regression is found, one way to
document it and be more confident that it will not come back is by writing a
unit test that covers it; write tests for code that is not yet covered by them,
making it possible to test it modularly or even be refactored.

