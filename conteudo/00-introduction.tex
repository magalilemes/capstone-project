%!TeX root=../tese.tex
%("dica" para o editor de texto: este arquivo é parte de um documento maior)
% para saber mais: https://tex.stackexchange.com/q/78101

%% ------------------------------------------------------------------------- %%

% "\chapter" cria um capítulo com número e o coloca no sumário; "\chapter*"
% cria um capítulo sem número e não o coloca no sumário. A introdução não
% deve ser numerada, mas deve aparecer no sumário. Por conta disso, este
% modelo define o comando "\unnumberedchapter".
\chapter{Introduction}

\enlargethispage{.5\baselineskip}
%% Contexto de software testing

Software testing is unquestionably a vital piece of software development. It
allows catching regressions early on, avoiding the costs of finding bugs later,
better ensures the overall software quality, and is one way to validate
changes made to the code. The greater the variety of good tests in the software,
the more robust it is.

Software testing becomes even more advantageous for projects with a massive
codebase. In the case of the Linux kernel, a free software project with over
20,000,000 lines of code and lots of developers from around the globe
contributing to it at a hectic pace, users could not be the only resource for
testing, so having a set of different automated tools to test it is beneficial
for the project.

Such a complex system naturally presents challenges when it comes to testing:
how can the developer ensure that a proposed change works across the many
different architectures, configurations, and hardware that Linux supports?
Performing these manual checks can be at times inconvenient. Thus, developers
can rely on the available testing tools in Linux or run their custom scripts.
Still, when it comes to testing, the main challenge mentioned by some of the
Linux kernel maintainers is the need for more hardware for it~\citep{schmitt:19}.

% siqueira: Essa afirmação é interessante e acho que esse parágrafo deveria se
% centrar nas dificuldades em testar o kernel.

Among all the different types of software testing, we highlight unit tests, a
way to test code by analyzing its internal logic, and, in most cases,
hardware-independent. Its advantage lies in the fact that they are relatively
faster to run compared to the other types of testing, providing quicker feedback
when detecting possible errors. Due to its significance, the Linux kernel has a
standardized framework for unit testing, named KUnit, used throughout this work
to implement unit tests in the kernel.

KUnit was merged into the kernel mainline, the main tree maintained by Linus
Torvalds, in 2019, making it a quite recent addition to the kernel. Its
interface makes writing unit tests across different subsystems easy and
uniform. The dependencies for running KUnit tests are the same as the Linux kernel,
making the learning curve for using this tool relatively gentle. However,
KUnit usage is still quite low in the kernel, with around 30\% of maintainers
stating that they never heard about it, according to a survey conducted
by~\cite{schmitt:19}.

In this work, we explored and understood how to implement unit tests within
Linux development context, focusing on unit tests using KUnit and its
features, since we believe a greater adoption of this tool can be very favorable
for the kernel. This work is closer to qualitative research, and the target of
our observations is to write tests for the display driver developed by AMD
(Advanced Micro Devices, Inc.). More specifically, we lean towards the
ethnography framework since we directly dealt with and interacted with the
community -- namely, the Linux community -- in which we were interested.

We deemed the AMD display driver subsystem a good candidate for introducing unit
tests for two reasons. The first one is due to the mentorship we had: one of our
mentors works at AMD, and the other two are external contributors to the same
subsystem we wanted to explore; this resulted in us being granted an AMD Radeon™
RX 5700 XT 50th Anniversary Graphics video card, displayed in Annex~\ref{picture}
as Figure~\ref{fig:amd-video-card} and now a property of the University of São
Paulo, to explore and learn more about its internals. The second one is because
the AMD display driver has an abundance of mathematical functions that we judged
to be good targets for covering with unit tests.

% Um bom caso para você citar aqui é a área automotiva. Nesse boom de "smart
% cars" tem sido bem comum sistemas de multimidia integrado e esses usam versões
% customizadas do Linux. Contudo, a área automotiva é extreamamente rigorosa e
% exige um elevado grau de confiabilidade. Mesmo que os sistemas multimidia não
% sejam diretamentamente ligado ao controle de bordo, existe uma pressão grande
% para mais testes no nível do kernel para ter alguma forma de dar mais
% confiabilidade. Ao meu ver, o Kunit pode ser uma resposta para essa demanda.

In ethnographic research, the researcher gets immersed in the field they are
studying, becoming its member and documenting what is learned along the way
from this fresh point of view~\citep{research-methods}. It can be achieved
through different means, such as participant observation and action research.
In participant observation, the researcher learns the practices of the group
being studied by engaging and interacting with it, also becoming a
practitioner. In action research, the researcher is motivated by promoting
change in the context of what is being studied. In contrast to participant
observation, this approach makes a clear distinction between the roles of
researcher and practitioner~\citep{participant-action}.

Another methodology that comes close to describing how we conducted this work
is the case study, where the researcher investigates specific events and the
circumstances brought by the practitioners~\citep{research-methods}. Since the
researcher is not necessarily directly involved in the experience being
studied, we determine this as a distinction from the ethnographic approach we
took. Our study mainly followed the participant-observation methodology by
participating in two groups.

\begin{center}
\begin{figure}[ht]
\begin{tikzpicture}
    \node [terminator] at (0,0) (start) {\textbf{Member}};
    \node [terminator] at (6,0) (community) {Linux kernel community};

     \path [-stealth, thick]
        (start) edge [bend left=0.5cm] node [above] {Send patch}   (community)
        (community) edge [bend left=0.5cm] node [below] {Discussion / Feedback}   (start);
\end{tikzpicture}
\caption{Direct interaction with the Linux kernel community}
\label{fig:linux-community}
\end{figure}
\end{center}

Making contributions to the Linux kernel is a challenging task. It requires
looking for a change, finding the correct repository to work on and mailing
lists to send the patch to, and setting up a development environment for
dealing with all of this. As we intended to contribute to writing unit tests,
we needed first to get used to this workflow and the community around it.
Initially, we made general contributions to the AMD display driver subsystem
and continued to do so throughout the project, interacting in a flow similar to
the one represented by Figure~\ref{fig:linux-community}.

\begin{center}
\begin{figure}[ht]
\begin{tikzpicture}
    \node [terminator] at (0,0) (start) {\textbf{Member}};
    \node [terminator] at (6,0) (group) {Group};
    \node [terminator] at (12,0) (community) {Linux kernel community};

     \path [-stealth, thick]
        (start) edge [bend left=0.4cm] node [above] {Send patch}   (group)
        (group) edge [bend left=0.4cm] node [below] {Discussion}   (start)
        (group) edge [bend left=0.4cm] node [above] {Send patch}   (community)
        (community) edge [bend left=0.4cm] node [below] {Discussion}   (group);
\end{tikzpicture}
\caption{Interaction with the Linux kernel community after internal discussions}
\label{fig:gsoc-community}
\end{figure}
\end{center}

Specific to our goal of introducing unit tests to the AMD display driver, we
were part of a group with contributors that took part in the Google Summer of
Code program and whose
projects\footnote{https://summerofcode.withgoogle.com/programs/2022/projects/fATmfPlL}\textsuperscript{,}\footnote{https://summerofcode.withgoogle.com/programs/2022/projects/6AoBcunH}\textsuperscript{,}\footnote{https://summerofcode.withgoogle.com/programs/2022/projects/JYeBJNnX}
had the same target as this study. In this smaller group, we internally
developed and discussed the infrastructure, design choices, and methodologies
for the tests before sending them to the Linux community for more feedback.
Figure~\ref{fig:gsoc-community} describes the flow of this interaction.

\begin{figure}[H]
    \includegraphics[scale=0.3]{figuras/display-contributions.png}
    \centering
    \caption{Comparison between AMD and external contributors in the AMD Display Driver}
    \label{fig:display-contributions}
\end{figure}

In summary, we can split the participant observation into two aspects. Firstly,
we contributed and interacted with the AMD display driver and KUnit community,
gathering their overall feedback about the tests we wrote and our choices.
Moreover, we made other contributions not directly related to the scope of this
work, but that served as a form of learning about Linux development workflow.
The second aspect is from being part of a smaller community of external
contributors to the AMD display driver, who were interested in introducing unit
tests into the subsystem, discussing and gathering lessons throughout the
process. Most AMD display driver subsystem contributions are from AMD
developers, as shown in Figure~\ref{fig:display-contributions} (obtained from
Program~\ref{prog:display-contributions}). Therefore, as external contributors,
proposing new changes in this environment can present some challenges.

The remainder of this capstone project consists of four more chapters.
Chapter~\ref{chap:background} explains the context where this work lies, going
over essential concepts related to the Linux kernel, software testing, in
particular, unit testing, and how the AMD display driver, our target of study,
is structured. Chapter~\ref{chap:results} mainly describes the steps to write
the unit tests and the design choices in adopting KUnit in a device driver. It
also contains the lessons we learned while implementing the tests.
Chapter~\ref{chap:conclusion} summarizes all the work and conclusions and
presents probable future works based on what was accomplished.
Chapter~\ref{chap:personal} covers the author's thoughts while working on the
project. Moreover, to help the reader, throughout the manuscript, we adopted
the following conventions: \textbf{bold} is used to highlight words;
\textit{italic} is used for file and directory names; \texttt{monospace} is
used for parts of code and commands.
