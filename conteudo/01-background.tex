%!TeX root=../tese.tex
%("dica" para o editor de texto: este arquivo é parte de um documento maior)
% para saber mais: https://tex.stackexchange.com/q/78101

\chapter{Background}
\label{chap:background}

This chapter provides the necessary information for understanding what was
accomplished in this work. We start with an introduction to the Linux kernel
and its development process, followed by an overview of some of the tools used
for testing it, then narrowing down to unit testing and how it is applied in
the context of the Linux kernel. Finally, we head over to the AMD display
driver, our object of study, to introduce unit tests.

\section{Linux Kernel Development}

The kernel is an essential piece of any operating system since it manages, at a
low level, the resources of a computer. In 1991, Linus Torvalds started
developing the Linux kernel as a hobby project based on MINIX. He published his
work on a MINIX newsgroup\footnote{https://historyofinformation.com/detail.php?entryid=2000}, asking for feedback, and, from
that moment on, Linux thrived as a free software project.
Nowadays, it powers smartphones, servers, desktops, and even embedded devices
such as Ingenuity, the first aircraft on Mars~\citep{helicopter}.

As for its organization, Linux is a monolithic kernel, meaning that the entire
operating system works in kernel mode~\citep{TanenbaumBos14}. At first glance,
this approach can lead to thinking that adding and removing a new feature is
complicated since all system procedures are linked into one single kernel
image, which would require changing the whole kernel. To overcome this, Linux
disposes of modules, which can be loaded and unloaded at runtime, making it
easy to extend the operating system.

The Linux kernel development style has unique features to coordinate the various
individuals and companies contributing to Linux. The Linux kernel is divided
into different areas with specific components such as file systems, memory
management, and device drivers. Within those areas, some subsystems specialize
even further into more specific domains. For instance, about file systems, there
are the \textit{ext4} and \textit{Btrfs} subsystems, and about GPUs drivers,
there are the AMD Display Core, Intel DRM Drivers, etc.
% s: http://www.tux-planet.fr/public/doc/whowriteslinux.pdf
% qq: italico para nome de ext4, btrfs?

Each kernel subsystem typically has its tree and development style. Changes
made into the kernel usually go first through the subsystem to which they
belong and are subsequently reviewed and merged by the subsystem
maintainer\(s\). Mailing lists are usually used for discussions and sending
patches, although there are subsystems that do not necessarily rely on them and
use other tools like GitHub. The \textit{MAINTAINERS} file available at the
kernel repository is the source for checking information about each subsystem.

\begin{figure}[ht]
\begin{lstlisting}[firstnumber=956,language={}]
AMD DISPLAY CORE
M:	Harry Wentland <harry.wentland@amd.com>
M:	Leo Li <sunpeng.li@amd.com>
M:	Rodrigo Siqueira <Rodrigo.Siqueira@amd.com>
L:	amd-gfx@lists.freedesktop.org
S:	Supported
T:	git https://gitlab.freedesktop.org/agd5f/linux.git
F:	drivers/gpu/drm/amd/display/
\end{lstlisting}
\caption{Section example from the MAINTAINERS file}
\label{fig:maintainers}
\end{figure}

An example extracted from the MAINTAINERS file is shown in
Figure~\ref{fig:maintainers}. The first line (AMD DISPLAY CORE) is the name of
the subsystem. The other lines are the section entries, listing information such
as the maintainers and their emails (M), relevant mailing list to send
patches and discussions (L), status (S), tree (T) where the subsystem work is
based and files (F) that the subsystem cover. Essentially, any change made in
files under \textit{drivers/gpu/drm/amd/display/} should go to the listed
maintainers and  mailing list, and also be compatible with the current state of
the indexed tree.

The mainline, the main kernel tree maintained by Linus, is where all of the work
developed in the subsystems comes together. There are some not-so-strict rules
in this development process: every two to three months, a kernel release is
made; most of the new changes introduced in the subsystems are merged during the
first two weeks of this cycle, called the merge window. After the merge window
closes, the subsequent development weeks prioritize patches that fix problems. A
new release candidate kernel is published roughly every new week until one of
them is deemed to be good enough for the final
release\footnote{https://www.kernel.org/doc/html/latest/process/2.Process.html}.
% talvez um diagrama aqui? https://lwn.net/Articles/670213/ {patch flow}

% siqueira: Não sei se da tempo, mas seria legal se tivesse um diagrama
% ilustrando algo prático usnado o amdgpu como exemplo. Aí dava para mostrar as
% contribuições entrando no amdgpu, aí o envio do amdgpu para o drm-misc-next e
% por fim o merge na branch do torvalds.

\section{Linux Kernel Testing Overview}

In the book \textit{The Art of Software Testing}, \cite{Myers.2012}
characterizes testing as the ``process of executing a program with the
intention of finding errors''. When bugs and regressions are discovered before
hitting production, developers can solve them faster, offering a friendlier
experience to the software end-user that will be less likely to deal with a
malfunctioning feature, for instance. Test is a common way to validate code,
mainly when new changes are introduced. Therefore, testing is undoubtedly an
essential step in software engineering practices.

Different types of tests exist, from techniques to testing the behavior of one
function exclusively up to testing an entire system. They each serve distinct
purposes and, when combined, help assure the reliability of a system.
~\cite{cohn09succeeding}, in his book ``Succeeding with Agile'', introduces the
concept of a test pyramid to categorize the different types of software testing:
(i) End-to-end tests are at the top of the pyramid and test the application as a
whole from the perspective of a user; (ii) Integration tests, responsible for
asserting the integration among the system components, are in the middle of
the pyramid; (iii) Unit tests, which test small units of the code, stand at the
base of the pyramid and are the focus of this project.

In the 6.0 Linux kernel release, more than 2,000 developers contributed to more
than 1,000,000 lines of code~\citep{kernel-6-stats}. Large projects where many
developers work on the same file set can be crowded and hectic, making
regressions and bugs almost unavoidable. That is why the Linux kernel counts not
only on its users for testing but also relies heavily on the aforementioned
automated testing tools.

Most of the millions of lines in the Linux kernel source code come from the
\textit{drivers} directory, as illustrated in
Figure~\ref{fig:kernel-directories}. Therefore, it is worth analyzing how this
part of the kernel is tested and which tools are used, as one of the goals of
this work is to introduce one form of testing into a specific device driver. In
this sense, \cite{schmitt:19} characterizes these testing tools,
distinguishing between those described in academic works and those found in grey
literature.

\begin{figure}[ht]
    \includegraphics[scale=0.4]{figuras/kernel-directories.png}
    \caption{Number of lines from the top-level directories of the 6.0 release kernel}
    \label{fig:kernel-directories}
\end{figure}

As for the mapped testing tools presented in academic papers, most of them
present issues when setting up, installing, and using. Compared to the academic
works, the number of testing tools described in the grey literature is much
larger. It is also a more reliable and up-to-date source to understand and
assess the tools that Linux developers currently use.

From the grey literature, there are reports from the different techniques
currently employed to test Linux: this varies from tools that can be used
locally by developers and others that report results after a patch is already
at the sent stage.

% Unit testing

% Regression testing

% Stress testing

% Functional testing

% Fuzz testing

% Reliability testing

% Robustness testing

% Stability testing

% Build testing

% Static analysis

% Performance testing

% Symbolic execution

% Fault injection

% Code Instrumentation

% Concolic Execution

% Local analysis and grouping

%% uma outra possibilidade para descrever o overview é listar todos os tipos de
%% teste, descrever e colocar as ferramentas que são do tipo


Regarding the tools developers use to test Linux locally, we highlight
Sparse\footnote{https://docs.kernel.org/dev-tools/sparse.html} and
Kselftest\footnote{https://docs.kernel.org/dev-tools/kselftest.html}, the most
commonly used tool by kernel maintainers, according to the survey conducted by
\cite{schmitt:19}. \textbf{Sparse}, similar to what
Smatch\footnote{https://lwn.net/Articles/691882/} and Coccinelle/coccicheck\footnote{https://docs.kernel.org/dev-tools/coccinelle.html} do,
is used for static analysis and can be easily run from inside the kernel
repository by running \texttt{make} with the corresponding option set.
\textbf{Kselftest} is a framework that can be used for writing many test types:
unit, regression, stress, functional, and performance. It is run as a userspace
process and is found at \textit{tools/testing/selftests} in the kernel source
code.

As for the tools that run integration tests and run them out of the developer
local scope, we mention the 0-day test robot\footnote{https://01.org/lkp/documentation/0-day-brief-introduction}, KernelCI\footnote{https://foundation.kernelci.org/}, and LKFT\footnote{https://lkft.linaro.org/}. The
\textbf{0-day test robot} makes use of some of the testing tools mentioned
above, retrieves patches that were sent to mailing lists, applies them to the
corresponding trees, compiles kernels with a variety of configurations, and
reports back to the developer that sent the patches in case of any failure or
warning. \textbf{KernelCI}, very similarly, also builds and runs tests against
a diverse set of architectures, providing detailed information about the
results. \textbf{Linux Kernel Functional Testing (LKFT)}, maintained by Linaro,
tests and builds kernels with a particular focus on the ARM architecture. In
summary, these platforms use some of the testing tools mentioned previously to
ensure the quality of the Linux kernel and detect regressions as early as
possible.

% exemplo de resultado do kernelci: https://linux.kernelci.org/test/job/mainline/branch/master/kernel/v6.1-rc7-200-gc2bf05db6c78/

% ferramentas que são reportadas como 'ativas'. Em parenteses as que foram mencionadas explicitamente
% (Kselftest), (KUnit)
% (LKFT), (0-Day)
% LTP
% Trinity, Syzkaller/Syzbot
% ktest, TuxMake, (KernelCI)
% (Smatch), (Coccinelle/coccicheck), (Sparse)
% FAUMachine

\section{Unit Testing}
% formal definition of unit testing
Unit testing, sometimes called module testing, is a software testing technique
that focuses on testing small code units. In particular, this type of testing
concentrates on single functions or even on their smaller, testable parts. As
this requires knowing the implementation -- as in code -- of the application to
be tested, it falls under the white-box testing category.

% historico
JUnit, the first framework specialized in unit testing for the Java language,
dates back to 1997~\citep{xunit}. Since then, many other frameworks have emerged, such as
pytest for Python and Jest for Javascript, offering a structure for concisely
writing unit tests.

% apresentar vantagens e desvantagens
 Because of their simple nature, unit tests are usually faster to run when
 compared to other types of testing, such as end-to-end testing. It also makes
 debugging more manageable, especially if the code to debug is covered by
 tests.

\subsection{Techniques for Building Test Cases}
\label{techniques-build-tests}

Several strategies help developers to think efficiently and write unit tests
for their code. \cite{Myers.2012} recommend using, firstly, white-box
strategies and, then, black-box strategies based on the code specification.  

\subsubsection{Black-Box Testing}

 \begin{itemize}   

   \item \textbf{Equivalence Partitioning}: takes advantage of the fact that
	   within the infinite number of possible inputs and outputs of the
		 software, some of those present the same behavior and can be
		 grouped into equivalent classes. By using this technique, we
		 can derive fewer test cases and remain confident that we are
		 still exercising and testing the program enough.

   \item \textbf{Boundary-value Analysis}: is often applied alongside the
	   equivalence partitioning method. This approach suggests analyzing
		 the class range and testing the values on its border and those
		 close to them. The motivation behind this is that more errors
		 are expected in areas where the software changes its behavior.
   %% TODO: não muito fã da escrita
   % https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=7845290

   \item \textbf{Error Guessing}: suggests guessing and writing tests for
	   special cases that a program may not have handled, such as, for a
		 program that receives a list as input, an empty list can be
		 provided as an input.   

 \end{itemize}

\subsubsection{White-Box Testing}


 \begin{itemize}   

   \item \textbf{Statement Coverage}: concerns having every code statement hit
	   at least once by testing. This is a fragile approach because one
		 piece of code could easily cover all of the statements and
		 still not cover many outputs.

     \item \textbf{Decision Coverage or Branch Coverage}: describes that the
	     true or false outcome of every conditional and loop should be
		 covered by testing.     

     \item \textbf{Multiple-condition Coverage}: tests all the combinations of
	     decisions within the program, expanding, even more, the strategy
		 applied by decision coverage.   

 \end{itemize}
% TODO: poderia detalhar melhor aqui

\subsection{Test-Driven Development (TDD)}

\begin{figure}[ht]
  \smartdiagram[circular diagram:clockwise]{Build test cases, Write the functionality, Refactor the code}
  \caption{TDD cycle}
  \label{fig:tdd-diagram}
\end{figure}

In a test-driven development procedure, developers first build the tests, and
since there is no functioning code, they fail. Only after the tests are written
the code, whose tests were just written, is developed so that the tests can
pass. This is done in a cycle, as illustrated in Figure~\ref{fig:tdd-diagram}
until the code is deemed good enough, contrary to the established development
workflow where features are first developed and tested. In these circumstances, unit tests are the type of tests that most fit TDD
purposes since they can easily be written before the feature is ready.

% https://martinfowler.com/bliki/TestDrivenDevelopment.html

\section{KUnit}

Unit tests existed in the kernel before KUnit, but for those subsystems that
used it, each had its way of handling and developing them. KUnit emerged as an
alternative to have a unified structure for unit tests within the kernel.
Google engineers developed it, the first RFC was sent on October
2018\footnote{https://lore.kernel.org/lkml/20181016235120.138227-1-brendanhiggins@google.com/},
and it was merged into the mainline in 2019, arriving in Linux v5.5.

% Daniel Vetter chama a situação de ter um monte de forma de ter teste unitário
% nos diferentes subsistemas de "home-grown solutions"
% https://www.spinics.net/lists/linux-kbuild/msg21671.html:
% "Plus gpu/drm isn't the only subsystem by far
% that already has a home-grown solution. So it's actually worse than
% what Ted said: We don't just have a multitude of test frameworks
% already, we have a multitude of ad-hoc unit test frameworks, each with
% their own way to run tests, write tests and mock parts of the system.
% Kunit hopefully helps us to standardize more in this area."

% As per marcelo's thesis, kunit is easy to run and configure.

%% kunit dependencies same as kernel
%% what makes it different from kselftest autotest, etc

KUnit tests can be wrapped in a module or be built-in: in the former, the tests are
run when the module is loaded, and in the latter, they are run during boot
time. When it started, KUnit enabled the unit tests to be run only using the
User-Mode Linux (UML) architecture, which allows running a kernel instance as a
standalone user-space process, meaning that there is no need to set up, build
and install a kernel on a target machine. This option still exists today, but
there is also support for running KUnit in other instances.
% https://docs.kernel.org/dev-tools/kunit/index.html#introduction

% is kunit integrated into any kernel (open) CIs?
%   - cki-project (?)
%   - Linux Kernel Functional Testing (yes)
%   - kernel-ci (no > issue aberta para adicionar https://github.com/kernelci/kernelci-core/issues/332)

% KUnit has also been integrated into Linaro's Linux Kernel Functional Test
% framework, which tests Linux's branches in various ways and serves as one
% mechanism to detect regressions quickly.

% https://qa-reports.linaro.org/lkft/linux-next-master/build/next-20221118/testrun/13058328/suite/kunit/tests/ < exemplo de testes do kunit não passando

%% architecture: https://docs.kernel.org/dev-tools/kunit/architecture.html

\subsection{Run KUnit}

%% need to enable the tests to be run first
%% select tests, what is a configuration file, how it impacts selecting tests
%% modprobe tests
To run KUnit tests, a Linux kernel repository, newer than version 5.5, is
needed. The dependencies for running KUnit are the same as the Linux kernel. The
most straightforward way to run KUnit is through the Python script, found under
\textit{tools/testing/kunit/kunit.py} in the kernel repository. This script can
configure, build and run a kernel with the selected KUnit tests on UML or a
virtual machine by using QEMU\footnote{Machine emulator and virtualizer}, adding
the possibility of running the tests in different architectures easily.

\begin{figure}[ht]
  \includegraphics[width=\textwidth]{figuras/kunit_drm_tests_result.png}
  \caption{Result output from running KUnit tests from the DRM subsystem}
  \label{fig:output-drm-tests}
\end{figure}

The selection of the tests to run is made through a configuration file that
tells the kernel which tests and how they should be built. It is a common
practice for subsystems that use KUnit to provide a \textit{.kunitconfig} file,
defining the tests to be built and the dependencies they may have. Then, when
running a set of tests from a specific subsystem is desired, we can pass that
configuration file as an argument for the \textit{kunit.py} script to execute,
as shown in Figure~\ref{fig:output-drm-tests}.

% ./tools/testing/kunit/kunit.py run --kunitconfig=drivers/gpu/drm/amd/display/tests/
%% snippet showing results

KUnit tests can also be run on real hardware. To do that, one must select which
tests to run through the system configuration file and build and install the
kernel onto the system. If the tests are in a module, running \texttt{modprobe
test\_module}, where \texttt{test\_module} is the name of the module containing
the tests, will load the tests and display the results in the kernel buffer. If
the tests are built-in, they automatically run during boot and, similarly, are
also available in the kernel buffer, which can be accessed by running
\texttt{dmesg}.

%% TODO: talvez uma figura mostrando o kernel buffer e como o resultado aparece


\subsection{Building Tests}
\label{building-tests}

The most basic idea in unit testing is making \textbf{assertions}, that is,
comparing whether an output is behaving accordingly to what is expected from
it. For making assertions, KUnit disposes of two classes of macros:
\texttt{KUNIT\_EXPECT\_*} and \texttt{KUNIT\_ASSERT\_*}. Assertions are
different from expectations: when assertions are not met, the test function
stops running completely, not allowing the other tests to be run. Some macros
such as \texttt{KUNIT\_EXPECT\_FALSE}, \texttt{KUNIT\_EXPECT\_TRUE} and
\texttt{KUNIT\_EXPECT\_NULL} check the state of only one value; others such as
\texttt{KUNIT\_EXPECT\_EQ}, \texttt{KUNIT\_EXPECT\_NE},
\texttt{KUNIT\_EXPECT\_STREQ} compare two values against each other.
% redudante falar isso? 

% siqueira: Acho que ficaria melhor se cada operação KUNIT_ tivesse o seu
% próprio bullet

In KUnit, functions of signature \texttt{void function\_test(struct kunit
*test)} define one \textbf{test case}, containing assertions and/or
expectations. \texttt{function\_test} is the name of the function containing
the assertions, and \texttt{struct kunit} is a structure that stores a test
context, with information such as the data needed for the test and the test
status. In Figure~\ref{fig:kunit-example-test-cases}, a simple example for
building tests for two functions using the \texttt{KUNIT\_EXPECT\_*} macros is
displayed.

\begin{figure}[ht]
  \begin{lstlisting}[language=C]
void prime_test(struct kunit *test)
{
    KUNIT_EXPECT_FALSE(test, is_prime(0));
    KUNIT_EXPECT_FALSE(test, is_prime(1));
    KUNIT_EXPECT_TRUE(test, is_prime(2));
    KUNIT_EXPECT_FALSE(test, is_prime(24));
}

void factorial_test(struct kunit *test)
{
    KUNIT_EXPECT_EQ(test, 1, factorial(0));
    KUNIT_EXPECT_EQ(test, 1, factorial(1));
    KUNIT_EXPECT_EQ(test, 2, factorial(2));
    KUNIT_EXPECT_EQ(test, 120, factorial(5));
    KUNIT_EXPECT_EQ(test, 3628800, factorial(10));
}
\end{lstlisting}
\caption{KUnit syntax for building two test cases}
\label{fig:kunit-example-test-cases}
\end{figure}

Each test case needs to be passed to the \texttt{KUNIT\_CASE} macro to set a
\texttt{struct kunit\_case} for that function. The \texttt{struct kunit\_case}
is a representation of the test function that stores additional information
such as the test status and log. Related functions are grouped in an array of
\texttt{struct kunit\_case}, as displayed in
Figure~\ref{fig:kunit-case-example}, required to end with \texttt{NULL}.

\begin{figure}[ht]
\begin{lstlisting}[language=C]
static struct kunit_case math_test_cases[] = {
    KUNIT_CASE(prime_test),
    KUNIT_CASE(factorial_test),
    {}
};
\end{lstlisting}
\caption{Test cases should be grouped into an array}
\label{fig:kunit-case-example}
\end{figure}

A \textbf{test suite}, defined as a \texttt{struct kunit\_suite}, is then built
with the array containing the test cases. We can optionally also specify
functions to be run before and after each test case by assigning a function to
\texttt{.init} and \texttt{.exit}, respectively. Functions for setting up and
tearing down the test environment before and after the test suite is run can
also be set in the \texttt{suite\_init} and \texttt{suite\_exit} members.
% https://kernel.org/doc/html/latest/dev-tools/kunit/style.html#suites
% https://elixir.bootlin.com/linux/latest/source/include/kunit/test.h#L170
Finally, for the tests in a suite to be recognized and run by KUnit, they must
be passed to the \texttt{kunit\_test\_suite} macro. Figure
~\ref{fig:kunit-suite} shows the declaration of a KUnit test suite built on top
of the previous examples.

\begin{figure}[ht]
\begin{lstlisting}[language=C]
static struct kunit_suite math_test_suite = {
    .name = "math",
    .init = math_test_init,
    .exit = math_test_exit,
    .suite_init = math_suite_init,
    .suite_exit = math_suite_exit,
    .test_cases = math_test_cases,
};
kunit_test_suite(math_test_suite);
\end{lstlisting}
\caption{KUnit suite declaration}
\label{fig:kunit-suite}
\end{figure}

Depending on how the test cases were built and grouped, there may be a need for
more than one test suite in one file. This can be achieved by simply replacing
\texttt{kunit\_test\_suite} with \texttt{kunit\_test\_suites} and passing all
the \texttt{struct kunit\_suite} as arguments.

KUnit also supports parameterized testing, providing a simple structure for
running the same set of tests for different values. Instead of having
\texttt{KUNIT\_CASE}, this approach uses \texttt{KUNIT\_CASE\_PARAM}, which
takes, besides the test cases, a function that iterates over the different cases as a second argument.
% https://kernel.org/doc/html/latest/dev-tools/kunit/usage.html#parameterized-testing

Testing every single function of a file through KUnit is not always possible in
a uniform way: there are different approaches depending on whether a function
is static. Currently, the only way to test static functions is by including the
\textit{.c} test file into the source file, using the \texttt{\#include}
directive. If we want to have a module only for tests, the functions to be
tested have to be exported so that the test module can access them. If there is
no such requirement and no static function to be tested, then the tests can be
a part of the module along with the functions to test.

% https://docs.kernel.org/dev-tools/kunit/architecture.html#test-cases
% KUNIT_EXPECT_*, for comparing the actual and expected behavior, and
% KUNIT_ASSERT_*, that interrupts all the tests if the expectation is not met.

% \begin{itemize}
%     \item KUNIT\_EXPECT\_*(): FALSE, TRUE, EQ, NE, LT, LE, GT, GE, STREQ, STRNEQ, NULL, NOT_NULL, NOT_ERR_OR_NULL, PTR_EQ, PTR_NEQ;
% \end{itemize}

%% https://docs.kernel.org/dev-tools/kunit/style.html

All of the macros, functions, and structs needed for building tests
mentioned previously are available in the \textit{kunit/test.h} header.
Therefore, every file containing KUnit tests has to include it.

\subsection{KUnit Usage in the Kernel}
\label{sec:kunit-usage}

KUnit adoption in the Linux kernel subsystems is still at a very initial stage.
Using the Linux kernel version 6.0 repository as a starting point, we built a
set of scripts to discover which subsystems use KUnit. The core idea to
determine that was by looking for all files that include the
\textit{kunit/test.h} header and, from there, obtain the subsystem to which
that file belongs.

% TODO: citar How familiar are you with these tools/testing infrastructure? KUnit do Marcelo?
In the Linux kernel version 6.0, we found 35 subsystems that adopted KUnit,
obtained through the script described in Program~\ref{prog:subsystems}. Apart
from the specific subsystems, KUnit is also used to test some general utility
libraries, such as hash routines and sorting, that do not belong to one
specific subsystem and are listed under ``THE REST''. From the list, we
highlight the tests developed for DRM, the parent subsystem of the AMD display
driver.

% \begin{table}
%   \resizebox{\textwidth}{!}{
%     \begin{tabular}{l | l}
%       APPARMOR SECURITY MODULE & KFENCE \\
%       ASPEED SD/MMC DRIVER & KPROBES \\
%       BITMAP API & LANDLOCK SECURITY MODULE \\
%       CHROME HARDWARE PLATFORM SUPPORT & LINEAR RANGES HELPERS \\
%       COMMON CLK FRAMEWORK & LIST KUNIT TEST \\
%       DATA ACCESS MONITOR & MANAGEMENT COMPONENT TRANSPORT PROTOCOL (MCTP) \\
%       DRIVER CORE, KOBJECTS, DEBUGFS AND SYSFS & NETWORKING [GENERAL] \\
%       DRM DRIVERS & NITRO ENCLAVES (NE) \\
%       EXT4 FILE SYSTEM & PROC SYSCTL \\
%       FILESYSTEMS (VFS and infrastructure) & REAL TIME CLOCK (RTC) SUBSYSTEM \\
%       GENERIC INCLUDE/ASM HEADER FILES & S390 \\
%       HIBERNATION (aka Software Suspend, aka swsusp) & SLAB ALLOCATOR \\
%       HID CORE LAYER & SOUND - SOC LAYER / DYNAMIC AUDIO POWER MANAGEMENT (ASoC) \\
%       IIO SUBSYSTEM AND DRIVERS & THE REST \\
%       KASAN & THUNDERBOLT DRIVER \\
%       KCSAN & TIMEKEEPING, CLOCKSOURCE CORE, NTP, ALARMTIMER \\
%       KERNEL UNIT TESTING FRAMEWORK (KUnit) & VFAT/FAT/MSDOS FILESYSTEM \\
%     \end{tabular}}
% \end{table}

\begin{multicols}{2}
  \begin{itemize}
    \item APPARMOR SECURITY MODULE
    \item ASPEED SD/MMC DRIVER
    \item BITMAP API
    \item CHROME HARDWARE PLATFORM SUPPORT
    \item COMMON CLK FRAMEWORK
    \item DATA ACCESS MONITOR
    \item DRIVER CORE, KOBJECTS, DEBUGFS AND SYSFS
    \item DRM DRIVERS
    \item EXT4 FILE SYSTEM
    \item FILESYSTEMS (VFS and infrastructure)
    \item GENERIC INCLUDE/ASM HEADER FILES
    \item HIBERNATION (aka Software Suspend, aka swsusp)
    \item HID CORE LAYER
    \item IIO SUBSYSTEM AND DRIVERS
    \item KASAN
    \item KCSAN
    \item KERNEL UNIT TESTING FRAMEWORK (KUnit)
    \item KFENCE
    \item KPROBES
    \item LANDLOCK SECURITY MODULE
    \item LINEAR RANGES HELPERS
    \item LIST KUNIT TEST
    \item MANAGEMENT COMPONENT TRANSPORT PROTOCOL (MCTP)
    \item NETWORKING [GENERAL]
    \item NETWORKING [MPTCP]
    \item NITRO ENCLAVES (NE)
    \item PROC SYSCTL
    \item REAL TIME CLOCK (RTC) SUBSYSTEM
    \item S390
    \item SLAB ALLOCATOR
    \item SOUND - SOC LAYER / DYNAMIC AUDIO POWER MANAGEMENT (ASoC)
    \item THE REST
    \item THUNDERBOLT DRIVER
    \item TIMEKEEPING, CLOCKSOURCE CORE, NTP, ALARMTIMER
    \item VFAT/FAT/MSDOS FILESYSTEM
  \end{itemize}
  \end{multicols}
  
  \begin{figure}[ht]
  \includegraphics[width=\textwidth]{figuras/kunit-usage.png}
  \caption{Amount of subsystems that adopted KUnit since its release}
  \label{fig:kunit-usage}
\end{figure}

% https://lwn.net/Articles/780985/ A kernel unit-testing framework


From the script in Program~\ref{prog:usage}, we obtained a plot, shown in
Figure~\ref{fig:kunit-usage}, illustrating the evolution in the number of subsystems
that have used KUnit since its release in v5.5.


\section{AMD Display Driver}

% GPU general vision
A Graphics Processing Unit (GPU) is a specific-purpose processor with many
small cores and a dedicated memory called VRAM -- which stands for Video RAM.
GPUs are mainly used in graphics, dealing with many simple calculations, such
as for rendering geometric shapes. As GPUs have many cores, they can execute
those many parallel simple operations more efficiently when compared to other
types of processors. The display driver performs the job of reading the GPU
frames in the VRAM up until displaying it on a screen or display device. % s2,s1
% s: Modern Operating Systems 4th edition
% s1: https://docs.kernel.org/gpu/amdgpu/display/index.html
% s2: https://people.freedesktop.org/~marcheu/linuxgraphicsdrivers.pdf

% Caracteriza o driver de display da AMD: complexidade ciclomática, número de linhas...
% TODO: characterize amd's display driver, some statistics like complexity, number of lines etc. biggest driver in the kernel
The AMD display driver in the Linux kernel is a part of the AMDGPU module and
is found under the \textit{drivers/gpu/drm/amd/display} directory. AMDGPU
belongs to the Direct Rendering Manager (DRM) subsystem, which is responsible
for handling graphics devices in Linux. The AMD display driver has two main
components\footnote{https://docs.kernel.org/gpu/amdgpu/display/index.html}:

 \begin{itemize}  

   \item \textbf{Display Core (DC)}: handles hardware programming and resource
	   management; the implementations do not depend on the operating
		 system it is on.  

   \item \textbf{Display Manager (DM)}: implements the AMDGPU base driver and
	   the DRM API, making it dependent on Linux.    

 \end{itemize}  

The driver supports two architectures: Display Core Engine
(DCE) and its successor, Display Core Next (DCN).
Figure~\ref{fig:amd-display-structure} shows the files and directories, in
yellow and blue respectively, that make up the AMD display driver, with the DM
component living under the \textit{amdgpu\_dm} folder and DC in the \textit{dc}
one.

\tikzset{
diretorio/.style={top color=white, bottom color=blue!20},
arquivo/.style={top color=white, bottom color=yellow!30}
}

\begin{center}
\begin{figure}[ht]
\begin{tikzpicture}[sibling distance=4.5em,
  every node/.style = {shape=rectangle, rounded corners,
    draw, align=center, scale=0.82}]]
  \node[diretorio] {drivers/gpu/drm/amd/display}
    child { node[diretorio] {\textbf{amdgpu\_dm}}}
    child { node[diretorio] {\textbf{dc}}}
    child { node[diretorio] {dmub}}
    child { node[diretorio] {include}}
    child { node[arquivo] {Kconfig}}
    child { node[arquivo] {Makefile}}
    child { node[diretorio] {modules}}
    child { node[arquivo] {TODO}};
\end{tikzpicture}
\caption{AMD display driver directory structure at kernel v6.0}
\label{fig:amd-display-structure}
\end{figure}
\end{center}


In the DCE architecture, fixed-point arithmetic is used to calculate mode
setting\footnote{Operation that sets a display resolution, depth, and
frequency.} parameters. The specific code for this architecture is spread
across the \textit{dc} directory, with a \textit{dce} folder centralizing the
common logic for the different versions of the architectures and specific
folders for each architecture version released: \textit{dce60} for the DCE 6
and \textit{dce120} for the DCE 12, for instance.

\tikzstyle{normal}=[draw=black,thick,anchor=west]
\tikzstyle{dc}=[draw=red,fill=red!30]
\tikzstyle{dml}=[dashed,fill=gray!50]

\begin{center}
  \begin{figure}[ht]
  \begin{tikzpicture}[
    grow via three points={one child at (0.5,-0.7) and
    two children at (0.5,-0.7) and (0.5,-1.4)},
    edge from parent path={(\tikzparentnode.south) |- (\tikzchildnode.west)}]
    \node [dc] {dc}
      child { node[normal] {basics}}
      child { node[normal] {dce}}
      child { node[normal] {dce60}}
      child { node[normal] {dce120}}
      child { node[normal] {dcn20}}
      child { node[normal] {dcn32}}
      child { node[normal] [dml] {dml}
        child { node[normal] {dcn20}}
        child { node[normal] {dcn32}}
      }
      child [missing] {}
      child [missing] {};
  \end{tikzpicture}
  \caption{DC simplified directory structure}
  \label{fig:dc-structure}
  \end{figure}
\end{center}



Alternately, the code for the DCN architecture uses floating-point
operations handled in the Floating-Point Unit (FPU). Floating-point usage in
the kernel can be unsafe, mainly due to it allowing a change of the
floating-point state of the CPU.
% s:
% https://web.archive.org/web/20140711155945/http://www.linuxsmiths.com/blog/?p=253
Because of this, the DCN code that uses FPU is isolated in the Display Mode
Library (DML) directory, found at \textit{dc}. Thus, it allows for centralized
control of the functions that access the FPU. Similar to the DCE case, the code
for each DCN version is under \textit{dc} -- \textit{dcn20} corresponds to the
DCN 2.0 architecture, as an example. There is the particularity of having
directories for each architecture at the \textit{dml} folder.
% s: https://patchwork.freedesktop.org/series/93042/ {drm/amd/display:
% Introduce FPU directory inside DC}

% siqueira: Talvez seja uma boa apresentar em um ou dois paragráfos o que é uma
% FPU do ponto de vista da arquitetura de computadores e quem sabe elaborar
% sobre o pq não é muito legal usar FPU no nível do kernel. Da uma olhada no
% primeiro capítulo desse livro:
% https://www.oreilly.com/library/view/understanding-the-linux/0596005652/

Another important directory under \textit{dc} is \textit{basics}, responsible
for providing a set of utility functions and structures, such as for handling
fixed-point operations and vectors. Both DCE and DCN rely on this module.

\begin{figure}[ht]
  \centering
  \begin{subfigure}{.5\textwidth}
    \centering
    \includegraphics[width=\textwidth]{figuras/amd-display-directories.png}
    \caption{Directories from the \textit{display} folder by number of lines}
    \label{fig:sub1}
  \end{subfigure}%
  \begin{subfigure}{.5\textwidth}
    \centering
    \includegraphics[width=\textwidth]{figuras/amd-display-dc-directories.png}
    \caption{Top 10 directories in \textit{dc} in number of lines}
    \label{fig:sub2}
  \end{subfigure}
  \caption{}
  \label{fig:display-directories}
\end{figure}

The Display Core directory has many other files and directories, such as the
graphics card BIOS and other architecture-specific files, that we will not
cover as they are only partially related to the scope of this work.
Figure~\ref{fig:dc-structure} shows a very simplified representation of the DC
directory structure, highlighting the \textit{dml} folder, where we wrote most
of the unit tests.

To get an idea of the size of the \textit{dc} directory, we created some scripts,
described in~\ref{prog:loc}, to obtain the number of lines of code for the
\textit{display} folders and for the \textit{dc} component.
Figure~\ref{fig:display-directories} depicts the size of the DC component in the
number of lines.

% https://www.amd.com/en/products/graphics/amd-radeon-rx-5700-xt-50th-anniversary

% https://lwn.net/Articles/708891/ {AMD's Display Core difficulties}
% https://docs.kernel.org/gpu/amdgpu/index.html
% https://docs.kernel.org/gpu/amdgpu/display/index.html
