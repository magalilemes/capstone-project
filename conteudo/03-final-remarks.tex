%!TeX root=../tese.tex
%("dica" para o editor de texto: este arquivo é parte de um documento maior)
% para saber mais: https://tex.stackexchange.com/q/78101

\chapter{Final Remarks} \label{chap:conclusion}   

This work presents a perspective on introducing unit tests in the Linux kernel,
focused on the AMD display driver subsystem and using the KUnit framework. The
first result of this work is the descriptions of techniques for designing unit
tests, which can be applied not only in the Linux kernel context but also in a
more general scope for other applications. The second result is more
kernel-centric and is about setting up a simple structure that allows for
testing any function in the code -- not limited to non-static ones -- and
making it easy for other developers to add any tests as needed.

Going through the formalism of designing test cases can be pretty valuable.
Approaches, such as equivalence partitioning and boundary-value analysis, help
in thinking and devising valuable tests and make this task less time-consuming
than blindly trying to devise the tests without any technique in mind. Tracking
and analyzing past regressions from the code that will be under test are also
very interesting ways to design test cases. These two forms of creating test
cases were used and discussed throughout this work and have found them to be
rich ways of documenting how a function behaves and how it has regressed in the
past.

Now that we have set up a structure for using KUnit in the AMD display driver,
as soon as our patches are merged, developers from the subsystem can take the
tests we wrote as example and write new tests, increasing the overall test
coverage of the subsystem. We recommend, for these new tests that may arise or
in other contexts out of the kernel, using the formal test design techniques and
covering regression, as they make the process of building tests less
complicated. Furthermore, having KUnit in the subsystem allows developers to
write tests for a feature that is not yet implemented, benefiting from the
Test-Driven Development process and, consequently, expanding the code coverage.

When compared to userspace applications, some particularities are related to
running the tests in the kernel. We tried to touch as little source code as
possible, meaning that we did not export any function we wanted to test. To
allow any function to be tested, we coupled the tests inside the AMDGPU module
-- so that the functions were visible to the test files -- but still had to
append the test file to the source files, which contained static functions we
desired to test. These choices are still subject to change.

Beyond the discussions brought in this monograph, we highlight these results and
contributions:
\begin{itemize}
    \item 1 tutorial at FLUSP website (Generate Linux kernel's KUnit test coverage reports\footnote{https://flusp.ime.usp.br/kernel/generate-kunit-test-coverage/}).
    \item 3 posts at personal blog\footnote{https://magalilemes.github.io/}.
    \item 11 authored patches to the Linux kernel\footnote{https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/log/?qt=author\&q=Magali}.
    \item 1 Reported-by patch \footnote{https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=01b537eeb049b98d7efc8f9c93c2608ef26ec338}.
    \item Full-slot presentation and lightning talk at the X.Org Developer's Conference 2022\footnote{https://indico.freedesktop.org/event/2/contributions/65/}\textsuperscript{,}\footnote{https://indico.freedesktop.org/event/2/contributions/164/}.
\end{itemize}

Moreover, as mentioned previously in Section ~\ref{sec:kunit-usage}, KUnit usage across
Linux kernel subsystems is still tiny, opening doors to some future works such
as:

\begin{itemize}
  
\item Adds more tests to subsystems that already use KUnit, improving their test coverage.
  
\item Measures the quality of the unit tests in the kernel, evaluating whether good practices in unit testing are followed.
  
\item Refactors code that is already covered by unit tests.
  
\item Analyzes the adoption of KUnit across the subsystems.

\end{itemize}   

We also emphasize two pending technical tasks directly related to this work and
points to be analyzed as future work:

\begin{itemize}
    
\item IGT GPU Tools\footnote{https://gitlab.freedesktop.org/drm/igt-gpu-tools} is a set of integration tests for the DRM subsystem. Among its tests, IGT can run  DRM unit tests defined inside the Linux kernel repository. Since DRM tests were converted to use KUnit, IGT had to be adapted to continue running these tests. As the AMD display driver is contained within DRM, its KUnit tests should also be run using IGT since it would make it easier to integrate it with existing CI infrastructures that already support IGT. There is already a work\footnote{https://groups.google.com/g/kunit-dev/c/fRteQ5\_6164} for adapting IGT to run KUnit tests, but it still lacks closure.
   
\item Test coverage can be used to detect missing coverage in the code and is a quite helpful metric to evaluate the progress of unit tests in a system. One advantage of KUnit is the ease of running its tests using the \textit{kunit\_tool} script available in the kernel repository. It is also possible to use this script with  gcov to generate the test coverage reports, but there are a few limitations for it:           

\begin{enumerate}
       
\item For GCC 9+ versions, there happens a linking issue: \texttt{mangle\_path} from \textit{gcc/gcov-io.*} conflicts with the function of the same name defined in the Linux kernel, \textit{fs/seq\_file.c}.
      
\item GCC 7+ versions are not able to generate gcov coverage information. This happens because of how gcov exit handler is dealt\footnote{https://lore.kernel.org/all/d36ea54d8c0a8dd706826ba844a6f27691f45d55.camel@sipsolutions.net/}.

\item UML\footnote{User-Mode Linux} is the only supported architecture for obtaining the test coverage using the script. The script does not support copying the files produced by gcov using QEMU \footnote{https://lore.kernel.org/all/CAGS\_qxpbH6c3OvoYZC6TXFQomLpwZg5q7=EZ9B9k=Rw1mOz=0w@mail.gmail.com/} as of now.

\end{enumerate}

\end{itemize}

Issues 1 and 2 sums up to the script only being able to generate test coverage
with a GCC version older than 7. GCC 7 was released back in 2017, and nowadays,
users have to compile and install these older versions from the source, which
can be a nuisance. From this, solving these first two issues would make it
pretty trivial to generate test reports using KUnit and gcov.  

Regarding issue 3, not all drivers support UML, which is the case with the AMD
display driver. To obtain the test coverage from this project using the
\textit{kunit\_tool}, we had to internally tweak the AMD display driver code a
bit to make it support
UML\footnote{https://gitlab.freedesktop.org/isinyaaa/linux/-/merge\_requests/8}.
Instead, by working on the way to retrieve the test coverage information from
other architectures through the script, we can guarantee a broader range of
subsystems that do not support UML being able to use \textit{kunit\_tool} to
obtain the test coverage.
